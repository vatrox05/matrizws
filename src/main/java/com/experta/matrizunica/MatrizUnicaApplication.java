package com.experta.matrizunica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatrizUnicaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MatrizUnicaApplication.class, args);
    }
}
