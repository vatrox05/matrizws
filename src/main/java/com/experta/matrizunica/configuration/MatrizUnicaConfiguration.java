package com.experta.matrizunica.configuration;


import com.experta.matrizunica.service.MatrizUnicaService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class MatrizUnicaConfiguration {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this package must match the package in the <generatePackage> specified in
        // pom.xml
        marshaller.setContextPath("com.experta.matrizunica.model");
        return marshaller;
    }

    @Bean
    public MatrizUnicaService matrizUnicaService(Jaxb2Marshaller marshaller) {
        MatrizUnicaService matrizUnicaService = new MatrizUnicaService();
        matrizUnicaService.setDefaultUri("http://osbdes.art.com:80/osb/art/srt/Establecimientos");
        matrizUnicaService.setMarshaller(marshaller);
        matrizUnicaService.setUnmarshaller(marshaller);
        return matrizUnicaService;
    }

}
