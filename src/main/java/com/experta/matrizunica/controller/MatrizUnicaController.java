package com.experta.matrizunica.controller;


import com.experta.matrizunica.model.ConsultaEstablecimientosPorCuit;
import com.experta.matrizunica.model.ConsultaEstablecimientosPorCuitResponse;
import com.experta.matrizunica.service.MatrizUnicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Vatrox10 on 7/31/2017.
 */
@RestController
public class MatrizUnicaController {

    @Autowired
    MatrizUnicaService matrizUnicaService;

    @GetMapping("/consultas/establecimientosporcuit/{cuit}")
    public ConsultaEstablecimientosPorCuitResponse consultaEstablecimientosPorCuit (@PathVariable("cuit") String cuit)
    {
        ConsultaEstablecimientosPorCuit consultaEstablecimientosPorCuit = new ConsultaEstablecimientosPorCuit();
        consultaEstablecimientosPorCuit.setCuit(cuit);

        ConsultaEstablecimientosPorCuitResponse consultaEstablecimientosPorCuitResponse = matrizUnicaService.consultaEstablecimientosPorCuit(consultaEstablecimientosPorCuit);


        return consultaEstablecimientosPorCuitResponse;
    }
}
