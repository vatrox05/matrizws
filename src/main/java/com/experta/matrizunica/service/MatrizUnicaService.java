package com.experta.matrizunica.service;

import com.experta.matrizunica.model.ConsultaEstablecimientosPorCuit;
import com.experta.matrizunica.model.ConsultaEstablecimientosPorCuitResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.springframework.stereotype.Service;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.springframework.xml.transform.StringSource;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

@Service
public class MatrizUnicaService extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(MatrizUnicaService.class);
    private String sistema = "SIART";
    private String environment ="TST";

    public MatrizUnicaService() {
    }

    public ConsultaEstablecimientosPorCuitResponse consultaEstablecimientosPorCuit (ConsultaEstablecimientosPorCuit consultaEstablecimientosPorCuit) {


        log.info("Requesting quote for " + consultaEstablecimientosPorCuit.getCuit());

        ConsultaEstablecimientosPorCuitResponse response = (ConsultaEstablecimientosPorCuitResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://osbdes.art.com:80/osb/art/srt/Establecimientos",
                         consultaEstablecimientosPorCuit,
                        new WebServiceMessageCallback() {

                            public void doWithMessage(WebServiceMessage message) {
                                try {
                                    SoapMessage soapMessage = (SoapMessage) message;
                                    soapMessage.setSoapAction("ConsultaEstablecimientosPorCuit");
                                    SoapHeader header = soapMessage.getSoapHeader();

                                    StringSource headerSource = new StringSource(
                                            "<sistema 	xmlns=\"http://www.art.com/servicios/commons/headers\">" + sistema + "</sistema>" +
                                                    "<env>" + environment + "</env>"
                                    );
                                    Transformer transformer = TransformerFactory.newInstance().newTransformer();
                                    transformer.transform(headerSource, header.getResult());

                                } catch (Exception e) {
                                    // exception handling
                                }
                            }
                        });

        return response;
    }

}
